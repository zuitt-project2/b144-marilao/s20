// JSON OBJECTS
// JSON stands for JS Object Notation
// JSON is used for serializing different data types into bytes.
// Serializationis the process of converting data into a series of bytes for easier transmission/transfer of info
// byte =? binary digits (1 and 0)that is used to represent a character

// SYNTAX: JSON DATA FORMAT
/*
	{
		"propertyA": "valueA",
		"propertyB": "valueB",
	}
*/

// JSON METHODS
// The JSON contains methods for parsing and converting datainto stringified JSON
/*
-Stringified JSON is a JS Object converted into a string to be used in ohter function of a JS application.
*/

let batchesArr = [
	{
		batchName: 'Batch X'
	},
	{
		batchName: 'Batch Y'
	}
]

console.log('Result from stringify method')

// The 'stringify' method is used to convert JS Objects into JSON(sttring)
console.log(JSON.stringify(batchesArr))

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})
console.log(data)

// Mini-Activity

let firstName = (prompt("Enter First Name: "));
let lastName = (prompt("Enter Last Name: "));
let age = parseInt(prompt("Enter Age: "));
let address = parseInt[prompt("City: "), prompt("Country: "), prompt("Zip Code: ")]

let userDetails = JSON.stringify({
	firstName:firstName,
	lastName:lastName,
	age: age,
	address: address
})
console.log(userDetails)


let batchesJSON = [
{
		"batchName": 'Batch X'
	},
	{
		"batchName": 'Batch Y'
	}
]
console.log('Result from parse method:')
console.log(JSON.parse(batchesJSON))